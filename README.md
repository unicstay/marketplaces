Marketplaces
============

Framework project for creating marketplaces

Development environment setup
-----

Run the following command:

```
PROJECTNAME=projectname
# clone this repository
git clone path/to/marketplaces.git

# initialize your project directory
mkdir mp-$PROJECTNAME
cd mp-$PROJECTNAME
virtualenv venv --python=python3
source venv/bin/activate

cd ../marketplaces
python setup.py develop
cd -
wagtail start $PROJECTNAME .

```