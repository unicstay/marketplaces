from django.db import models
from django.utils.translation import ugettext_lazy as _

from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailcore.models import Page

from .mixins import (
    ImportedMixin,
    LabeledMixin,
    PositionedMixin,
)


class AbstractSeller(LabeledMixin, ImportedMixin, models.Model):

    panels = LabeledMixin.panels

    class Meta:
        abstract = True


class AbstractProduct(LabeledMixin, ImportedMixin, models.Model):

    panels = LabeledMixin.panels

    class Meta:
        abstract = True


class AbstractProductPage(LabeledMixin, Page):

    content_panels = Page.content_panels + [
        FieldPanel('label'),
    ]

    class Meta:
        abstract = True


class AbstractSellerPage(LabeledMixin, Page):

    content_panels = Page.content_panels + [
        FieldPanel('label'),
    ]

    class Meta:
        abstract = True


class AbstractPoi(LabeledMixin, PositionedMixin, models.Model):

    default_search_radius = models.IntegerField(_('default search radius'),
        null=True,
        help_text=_('default search on this POI will return everything in `default search radius` kilometers'))

    panels = ([] +
        LabeledMixin.panels +
        PositionedMixin.panels +
        [FieldPanel('default_search_radius'), ])

    class Meta:
        abstract = True
