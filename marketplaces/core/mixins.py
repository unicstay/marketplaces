import logging

from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.translation import ugettext_lazy as _

from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel


log = logging.getLogger(__name__)


class UnderscoredLookupMixin(object):

    def get(self, path):
        raise NotImplementedError()


#
# Mixins for pages
#

class DescribedMixin(models.Model):

    short_description = models.TextField(blank=True)
    detailed_description = models.TextField(blank=True)

    content_panels = [
        MultiFieldPanel([
            FieldPanel('short_description'),
            FieldPanel('detailed_description'),
        ], _("Descriptions"))
    ]

    class Meta:
        abstract = True


class ImportedRelatedMixin(UnderscoredLookupMixin):

    imported = None

    def get(self, path):
        components = path.split('__')
        if len(components) == 1 and hasattr(self, components[0]):
            return getattr(self, components[0])

        if self.imported is None:
            raise Exception('please define the "imported" class attribute')

        return getattr(self, self.imported).get(path)


#
# Mixins for models
#

class AddressableMixin(models.Model):
    address_1 = models.CharField(max_length=255)
    address_2 = models.CharField(max_length=255, blank=True, default='')
    address_3 = models.CharField(max_length=255, blank=True, default='')
    zipcode = models.CharField(max_length=50)
    city = models.CharField(max_length=255)
    country = models.CharField(max_length=255)

    panels = [MultiFieldPanel([
        FieldPanel("address_1"),
        FieldPanel("address_2"),
        FieldPanel("address_3"),
        FieldPanel("zipcode"),
        FieldPanel("city"),
        FieldPanel("country")],
        _("Address"))]

    class Meta:
        abstract = True


class AdministeredMixin(models.Model):
    ad_1 = models.CharField(max_length=255)
    ad_2 = models.CharField(max_length=255)

    panels = [MultiFieldPanel([
        FieldPanel("ad_1"),
        FieldPanel("ad_2")],
        _("Administrative levels"))]

    class Meta:
        abstract = True


class LabeledMixin(models.Model):

    label = models.CharField(_('label'),
        max_length=255)

    def __str__(self):
        return self.label

    panels = [FieldPanel("label")]

    class Meta:
        abstract = True


class ImportedMixin(UnderscoredLookupMixin, models.Model):

    data = JSONField(blank=True, default=dict)

    def get(self, path):
        components = path.split('__')
        if len(components) == 1 and hasattr(self, components[0]):
            return getattr(self, components[0])

        try:
            piece = self.data
            for item in components:
                try:
                    index = int(item)
                except ValueError:
                    index = item
                piece = piece[index]
            return piece
        except KeyError:
            return None
        except TypeError:
            return None

    class Meta:
        abstract = True


class PositionedMixin(models.Model):
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    panels = [MultiFieldPanel([
        FieldPanel("latitude"),
        FieldPanel("longitude")],
        _("GPS Position"))]

    class Meta:
        abstract = True
