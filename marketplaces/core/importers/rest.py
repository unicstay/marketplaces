import logging
import requests

from .base import AbstractReader


logger = logging.getLogger(__name__)


class RestReader(AbstractReader):

    def __init__(self, endpoint, credentials, paginated=False, **kwargs):
        self.endpoint = endpoint
        self.credentials = credentials
        self.paginated = paginated
        super(RestReader, self).__init__(**kwargs)

    def fetch(self):
        if not self.paginated:
            return self._fetch(self.endpoint, self.credentials)
        else:
            data = []
            url = self.endpoint
            while url:
                piece = self._fetch(url, self.credentials)
                data += piece.get('results', [])
                url = piece.get('next', None)
            return data


    @staticmethod
    def _fetch(url, credentials):
        response = requests.get(url, auth=credentials)
        if response.status_code == 200:
            return response.json()
        else:
            logger.info('request to {} with {} returned {} {}'.format(
                url,
                credentials,
                response.status_code,
                response.text))
            return {}

    def read(self):
        data = self.fetch()
        for item in data:
            yield item
