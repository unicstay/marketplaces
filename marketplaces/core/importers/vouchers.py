from collections import defaultdict

from .base import AbstractReader
from .rest import RestReader


class VoucherReader(AbstractReader):

    def __init__(self, endpoint, **kwargs):
        self.endpoint = endpoint
        self.prototype_reader = RestReader(endpoint + '/voucherprototypes/', None, paginated=True)
        self.partner_reader = RestReader(endpoint + '/partners/{}/', None)
        self.images_reader = RestReader(endpoint + '/voucherprototypeimages/', None)
        self.options_reader = RestReader(endpoint + '/options/{}/', None)
        self.options_reader_pattern = endpoint + '/options/{}'
        super(VoucherReader, self).__init__(**kwargs)

    def get_option(self, options, option_id):
        if option_id not in options:
            self.options_reader.endpoint = self.options_reader_pattern.format(option_id)
            options[option_id] = self.options_reader.fetch()
        return options[option_id]

    def read(self):
        partners_info = None
        images = defaultdict(list)
        options = {}
        for image in self.images_reader.fetch():
            images[image['voucher_prototype']].append(image)
        prototypes = self.prototype_reader.fetch()

        for item in prototypes:
            if partners_info is None and item['partners_info']:
                partner_id = item['partners_info'][0]['id']
                self.partner_reader.endpoint = self.partner_reader.endpoint.format(partner_id)
                partners_info = self.partner_reader.fetch()
            prototype_included_options = []
            for option in item['included_options']:
                o = self.get_option(options, option['id'])
                prototype_included_options.append({'name': o['name'],
                                                    'description': o['description']})
            item['included_options'] = prototype_included_options
            prototype_available_options = []
            for option in item['available_options']:
                o = self.get_option(options, option)
                prototype_available_options.append({'name': o['name'],
                                                    'price': o['price'],
                                                    'is_active': o['is_active']})
            item['available_options'] = prototype_available_options
            item['partners_info'] = partners_info
            item['images'] = images[item['id']]
            yield item
