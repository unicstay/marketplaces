import logging


logger = logging.getLogger(__name__)


class AbstractReader(object):

    def read(self):
        """this is supposed to be a generator function, it should yield!"""
        raise NotImplementedError()


class AbstractWriter(object):

    def write(self, item):
        raise NotImplementedError()


class Importer(object):

    reader_class = AbstractReader
    writer_class = AbstractWriter

    def __init__(self, read_params, write_params):
        self.read_params = read_params
        self.write_params = write_params

    def get_reader(self):
        return self.reader_class(**self.get_reader_kwargs())

    def get_writer(self):
        return self.writer_class(**self.get_writer_kwargs())

    def get_reader_kwargs(self):
        return self.read_params

    def get_writer_kwargs(self):
        return self.write_params

    def before_hook(self):
        pass

    def after_hook(self):
        pass

    def run(self):
        reader = self.get_reader()
        writer = self.get_writer()
        self.before_hook()
        for item in reader.read():
            writer.write(item)
        self.after_hook()
