from django.db import models
from django.utils.translation import ugettext_lazy as _

from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel


class ContactSettings(models.Model):

    displayed_phone = models.CharField(
        max_length=255,
        blank=True, default='',
        help_text=_('The phone number you want to see displayed on your website'))
    displayed_email = models.EmailField(
        blank=True, default='',
        help_text=_('The email you want to to see displayed on your website'))
    notification_email = models.EmailField(
        blank=True, default='',
        help_text=_('Email sent from your website will originate from this address'))
    sender_name = models.CharField(
        blank=True, default='',
        max_length=255,
        help_text=_('Email sent from your website will have this sender'))

    panels = [
        MultiFieldPanel([
            FieldPanel('displayed_phone'),
            FieldPanel('displayed_email'),
            FieldPanel('notification_email'),
            FieldPanel('sender_name'),
        ], _('Contact'))
    ]

    class Meta:
        abstract = True


class EditorSettings(models.Model):

    favicon = models.ImageField(
        blank=True, null=True)
    footer_text = models.CharField(
        max_length=255,
        blank=True, default='')
    robots = models.TextField(
        blank=True, default='')

    panels = [
        MultiFieldPanel([
            FieldPanel('favicon'),
            FieldPanel('footer_text'),
            FieldPanel('robots'),
        ], _('Editor'))
    ]

    class Meta:
        abstract = True


class ServicesSettings(models.Model):

    class Meta:
        abstract = True


class SocialMediaSettings(models.Model):

    twitter = models.URLField(
        blank=True, default='',
        help_text=_('Your twitter page URL'))
    facebook = models.URLField(
        blank=True, default='',
        help_text=_('Your Facebook page URL'))
    google_plus = models.URLField(
        blank=True, default='',
        help_text=_('Your Google+ page URL'))
    pinterest = models.URLField(
        blank=True, default='',
        help_text=_('Your Pinterest page URL'))
    instagram = models.CharField(
        max_length=255,
        blank=True, default='',
        help_text=_('Your Instagram username, without the @'))
    rss = models.URLField(
        blank=True, default='',
        help_text=_('Your main rss feed'))
    youtube = models.URLField(
        blank=True, default='',
        help_text=_('Your YouTube channel or user account URL'))

    panels = [
        MultiFieldPanel([
            FieldPanel('twitter'),
            FieldPanel('facebook'),
            FieldPanel('google_plus'),
            FieldPanel('pinterest'),
            FieldPanel('instagram'),
            FieldPanel('rss'),
            FieldPanel('youtube'),
        ], _('Social media'))
    ]

    class Meta:
        abstract = True
