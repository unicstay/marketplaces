template = """version: '2'
services:
  db:
    extends:
      file: commonservices.yml
      service: db
  search:
    extends:
      file: commonservices.yml
      service: search
  app:
    build: .
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/code
    ports:
      - "8000:8000"
    depends_on:
      - db
      - search
"""


commonservices = """version: '2'
services:
  db:
    image: postgres
  search:
    image: elasticsearch:1.7
"""


dockerfile = """FROM python:3.5
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
"""
