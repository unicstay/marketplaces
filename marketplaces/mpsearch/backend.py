from django.db.models import Q
from django.db.models import fields
from django.db.models.expressions import Expression
from django.db.models.query import QuerySet

from wagtail.wagtailsearch.backends.base import FieldError
from wagtail.wagtailsearch.backends.elasticsearch2 import SearchBackend as ElasticsearchSearchBackend
from wagtail.wagtailsearch.index import class_is_indexed


__all__ = ['SearchBackend']


class Target(object):
    def __init__(self, attname):
        self.attname = attname


class E(Expression):
    def __init__(self, filter_field_name, output_field=None):
        if output_field is None:
            output_field = fields.Field()

        self.filter_field_name = filter_field_name
        self.target = Target(filter_field_name)
        super(E, self).__init__(output_field=output_field)

    def __repr__(self):
        return "{} ({})".format(self.__class__.__name__, self.filter_field_name)

    def as_sql(self, compiler, connection):
        return '(1)', ()

    def get_group_by_cols(self):
        return [self]


class MarketplaceElasticSearchResults(ElasticsearchSearchBackend.results_class):

    def __init__(self, backend, query, base_queryset=None, prefetch_related=None):
        self.base_queryset = base_queryset or query.queryset
        super(MarketplaceElasticSearchResults, self).__init__(backend, query, prefetch_related=prefetch_related)

    def _clone(self):
        klass = self.__class__
        new = klass(self.backend, self.query, base_queryset=self.base_queryset, prefetch_related=self.prefetch_related)
        new.start = self.start
        new.stop = self.stop
        return new

    def _do_search(self):
        # Params for elasticsearch query
        params = dict(
            index=self.backend.get_index_for_model(self.query.queryset.model).name,
            body=self._get_es_body(),
            _source=False,
            fields='pk',
            from_=self.start,
        )

        # Add size if set
        if self.stop is not None:
            params['size'] = self.stop - self.start

        # Send to Elasticsearch
        hits = self.backend.es.search(**params)

        # Get pks from results
        pks = [hit['fields']['pk'][0] for hit in hits['hits']['hits']]

        # Initialise results dictionary
        results = dict((str(pk), None) for pk in pks)

        # Find objects in database and add them to dict
        queryset = self.base_queryset.filter(pk__in=pks)
        for obj in queryset:
            results[str(obj.pk)] = obj

        # Return results in order given by Elasticsearch
        return [results[str(pk)] for pk in pks if results[str(pk)]]


class MarketplaceElasticsearchSearchBackend(ElasticsearchSearchBackend):
    results_class = MarketplaceElasticSearchResults

    def filtered_search(self, query_string, model_or_queryset, fields=None, filters=None,
               prefetch_related=None, operator=None, order_by_relevance=True,
               extra_filters=None):
        # Find model/queryset
        if isinstance(model_or_queryset, QuerySet):
            model = model_or_queryset.model
            queryset = model_or_queryset
        else:
            model = model_or_queryset
            queryset = model_or_queryset.objects.all()

        # Model must be a class that is in the index
        if not class_is_indexed(model):
            return []

        # Check that theres still a query string after the clean up
        if query_string == "":
            return []

        # Only fields that are indexed as a SearchField can be passed in fields
        if fields:
            allowed_fields = {field.field_name for field in model.get_searchable_search_fields()}

            for field_name in fields:
                if field_name not in allowed_fields:
                    raise FieldError(
                        'Cannot search with field "' + field_name + '". Please add index.SearchField(\'' +
                        field_name + '\') to ' + model.__name__ + '.search_fields.'
                    )

        # Apply filters to queryset
        if filters:
            queryset = queryset.filter(**filters)

        # Prefetch related
        if prefetch_related:
            for prefetch in prefetch_related:
                queryset = queryset.prefetch_related(prefetch)

        # Check operator
        if operator is not None:
            operator = operator.lower()
            if operator not in ['or', 'and']:
                raise ValueError("operator must be either 'or' or 'and'")

        # Annotate and filter with extra filters
        filtered_queryset = queryset
        for filter_name, filter_value in extra_filters.items():
            filter_field_name = filter_name.split('__')[0]
            annotate_params = {filter_field_name: E(filter_field_name)}
            filter_params = {filter_name: filter_value}
            filtered_queryset = filtered_queryset.annotate(**annotate_params).filter(**filter_params)

        # Search
        search_query = self.query_class(
            filtered_queryset, query_string, fields=fields, operator=operator, order_by_relevance=order_by_relevance,
        )
        return self.results_class(self, search_query, base_queryset=queryset)


SearchBackend = MarketplaceElasticsearchSearchBackend
