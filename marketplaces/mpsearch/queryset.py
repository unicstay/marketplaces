from __future__ import absolute_import, unicode_literals

from wagtail.wagtailcore.query import PageQuerySet
from wagtail.wagtailcore.models import PageManager as WagtailPageManager
from wagtail.wagtailsearch.backends import get_search_backend


class FilteredSearchableQuerySetMixin(object):
    def filtered_search(self, query_string, fields=None,
               operator=None, order_by_relevance=True, backend='default',
               **search_filters):
        """
        This runs a search query on all the items in the QuerySet
        """
        search_backend = get_search_backend(backend)

        if hasattr(search_backend, 'filtered_search'):
            return search_backend.filtered_search(
                query_string, self, fields=fields,
                operator=operator, order_by_relevance=order_by_relevance,
                extra_filters=search_filters)
        else:
            return search_backend.search(query_string, self, fields=fields,
                                         operator=operator, order_by_relevance=order_by_relevance)


class FilteredSearchablePageQuerySet(FilteredSearchableQuerySetMixin, PageQuerySet):
    pass


class BasePageManager(WagtailPageManager):

    def get_queryset(self):
        return FilteredSearchablePageQuerySet(self.model).order_by('path')


PageManager = BasePageManager.from_queryset(FilteredSearchablePageQuerySet)
