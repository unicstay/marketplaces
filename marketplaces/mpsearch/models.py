from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import models
from django.http import QueryDict

from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailcore.models import Page
from wagtail.wagtailsearch.models import Query


class BaseSearchPage(Page):

    template = 'search/search.html'
    queryset = Page.objects.live()
    number_of_results = 10
    filter_form_class = None
    form_prefix = 'filter_form'
    empty_means_all = True

    custom_query = models.CharField(max_length=255, blank=True, default='')

    content_panels = (
        Page.content_panels +
        [FieldPanel('custom_query')]
    )

    def get_queryset(self):
        return self.queryset

    def get_filter_form(self, request=None):
        params = self.get_filter_form_params(request)
        form = None
        if self.filter_form_class is not None:
            params = {key: value for key, value in params.items() if key.replace(self.form_prefix + '-', '') in self.filter_form_class.declared_fields}
            for field_name, field in self.filter_form_class.declared_fields.items():
                prefixed_field_name = '{}-{}'.format(self.form_prefix, field_name)
                if prefixed_field_name not in params:
                    params[prefixed_field_name] = field.initial
            form = self.filter_form_class(params, prefix=self.form_prefix)
        return form

    def get_filter_form_params(self, request=None):
        query_params = request.GET.copy() if request is not None else QueryDict().copy()
        if self.custom_query:
            query_params.update(QueryDict(self.custom_query))
        return query_params

    def get_filters(self, form):
        filters = {}
        if form is not None and form.is_valid():
            filters = form.cleaned_data

        filters = {k: v for k, v in filters.items() if v is not None and v != ''}
        return filters

    def get_search_method(self, filters):
        return 'filtered_search' if filters else 'search'

    def do_search(self, query, filters):
        filters = filters or {}
        return getattr(self.get_queryset(), self.get_search_method(filters))(query, **filters)

    def get_search_context(self, request, *args, **kwargs):
        search_query = request.GET.get('query', None)
        if self.empty_means_all and not search_query:
            search_query = None
        page = request.GET.get('page', 1)

        filter_form = self.get_filter_form(request=request)
        search_results = self.do_search(search_query, self.get_filters(filter_form))
        if search_query:
            query = Query.get(search_query)
            # Record hit
            query.add_hit()

        # Pagination
        paginator = Paginator(search_results, self.number_of_results)
        try:
            search_results = paginator.page(page)
        except PageNotAnInteger:
            search_results = paginator.page(1)
        except EmptyPage:
            search_results = paginator.page(paginator.num_pages)

        return {
            'search_query': search_query,
            'search_results': search_results,
            'filter_form': filter_form,
        }

    def get_context(self, request, *args, **kwargs):
        context = super(BaseSearchPage, self).get_context(request, *args, **kwargs)
        context.update(self.get_search_context(request, *args, **kwargs))

        return context

    class Meta:
        abstract = True
