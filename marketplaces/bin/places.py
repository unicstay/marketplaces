#!/usr/bin/env python

import sys
import argparse
import os

import marketplaces.scaffholding

from django.core.management import ManagementUtility


def generate_from_template(template, output):
    with open(output, 'w') as f:
        f.write(template)


def generate_commonservices(args):
    generate_from_template(
        marketplaces.scaffholding.commonservices,
        'commonservices.yml')


def generate_compose(args):
    generate_from_template(
        marketplaces.scaffholding.template,
        'docker-compose.yml')


def generate_dockerfile(args):
    generate_from_template(
        marketplaces.scaffholding.dockerfile,
        'Dockerfile')


def start_project(args):
    project_name = args.project_name[0]
    dest_dir = args.directory
    # Make sure given name is not already in used
    # by another python package/module.
    try:
        import project_name
    except ImportError:
        pass
    else:
        sys.exit("'{}' conflicts with the name of an existing "
                 "Python module and cannot be used as a project "
                 "name. Please try another name.".format(project_name))

    print('Creating a Marketplaces project called {}'.format(project_name))

    # First find the path to Marketplaces
    import marketplaces
    marketplaces_path = os.path.dirname(marketplaces.__file__)
    template_path = os.path.join(marketplaces_path, 'project_template')

    # Call django-admin startproject
    utility_args = ['django-admin.py',
                    'startproject',
                    '--template=' + template_path,
                    '--ext=html,rst',
                    project_name]

    if dest_dir is not None:
        utility_args.append(dest_dir)

    utility = ManagementUtility(utility_args)
    utility.execute()

    print("Success! {} has been created".format(project_name))


def main():
    # Parse options
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    sp_generate = subparsers.add_parser('generate').add_subparsers()
    sp_generate_commonservices = sp_generate.add_parser('commonservices')
    sp_generate_commonservices.set_defaults(func=generate_commonservices)
    sp_generate_compose = sp_generate.add_parser('compose')
    sp_generate_compose.set_defaults(func=generate_compose)
    sp_generate_dockerfile = sp_generate.add_parser('dockerfile')
    sp_generate_dockerfile.set_defaults(func=generate_dockerfile)

    p_start = subparsers.add_parser('start')
    p_start.add_argument(
        'project_name',
        nargs=1,
        help='Project name for your marketplaces installation'
    )
    p_start.add_argument(
        'directory',
        nargs='?',
        help='Destination directory of your new project'
    )
    p_start.set_defaults(func=start_project)

    args = parser.parse_args()
    if hasattr(args, 'func'):
        args.func(args)
    else:
        parser.print_help()

if __name__ == "__main__":
    main()
