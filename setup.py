#!/usr/bin/env python

import os.path
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()


setup(
    name='marketplaces',
    version='0.1.0',
    find_packages=find_packages(),
    include_package_data=True,
    license='All rights reserved Unic Stay',
    description='framework for building marketplaces',
    long_description=README,
    author='Unic Stay',
    author_email='dev@unicstay.com',
    install_requires=[
        'Django>=1.9, <1.12',
        'wagtail==1.10.1',
    ],
    entry_points="""
            [console_scripts]
            places=marketplaces.bin.places:main
    """,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ]
)
